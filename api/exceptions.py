class EntityDoesNotExist(Exception):
    pass


class NoMediafiles(Exception):
    pass
